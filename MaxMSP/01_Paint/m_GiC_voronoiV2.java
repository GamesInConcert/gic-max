import com.cycling74.max.*;

public class m_GiC_voronoiV2 extends MaxObject
{
	private int n_z;
	private int n_x_max = 20, n_x;
	private float fi, d_z = 1, modX = 0.0f, modY = 0.0f;
	private float[][] x = new float[3][n_x_max];
	private float[][] z;// = new float[2][n_z];
	private float PI = 3.1415926f;
	private float[] dist = new float[n_z];

	private static final String[] INLET_ASSIST = new String[]{
		"float (x1 y1 faktor1 x2 y2 faktor2 ...)","Winkel in radians","distanz zwischen Punkten auf Strahl"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"liste der k�rzesten distanzen"
	};
	
	public m_GiC_voronoiV2(int n) //Atom[] args
	{
		declareInlets(new int[]{DataTypes.ALL,DataTypes.FLOAT,DataTypes.FLOAT,DataTypes.FLOAT,DataTypes.FLOAT});
		declareOutlets(new int[]{DataTypes.ALL});
		
		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);

		n_z = n;
		z = new float[2][n_z];

	}
    
	public void bang()
	{
	}
    
	public void inlet(int i)
	{
	}
    
	public void inlet(float f)
	{
	  if(getInlet() == 1)
	  {
		fi = f;
	//	calc_distSimple();
		calc_z();
		calc_dist();
	  }
	  if(getInlet() == 2)
	  {
		d_z = f;
	//	calc_distSimple();
	//	calc_z();
	//	calc_dist();
	  }
	  if(getInlet() == 3)
	  {
		modX = f;
	//	calc_z();
	//	calc_dist();
	  }
	  if(getInlet() == 4)
	  {
		modY = f;
	//	calc_z();
	//	calc_dist();
	  }
	}
    
    
	public void list(Atom[] list)
	{
		n_x = list.length/3;
		Atom a;
		for(int i = 0; i < n_x; i++)
		{
			a = list[3*i];
			x[0][i] = a.getFloat();
			a = list[3*i+1];
			x[1][i] = a.getFloat();
			a = list[3*i+2];
			x[2][i] = a.getFloat();
		}
		calc_dist();

	}   

int calc_dist()
{
	float[] _dist = new float[n_z];
	float dist, min_dist;
	int myAtom = 0;
	for(int k = 0; k < n_z; k++)		// �ber alle punkte z
	{
		min_dist = 1000;
		for(int i = 0; i < n_x; i++)		// �ber alle punkte x
		{
			//dist = (float)Math.sqrt( (z[0][k]-x[0][i])*(z[0][k]-x[0][i]) + (z[1][k]-x[1][i])*(z[1][k]-x[1][i]));
			dist = (float)Distance_minkowski(z[0][k], z[1][k], x[0][i], x[1][i], 2.0);
			if(dist < min_dist){
				min_dist = dist;
				myAtom = i;
			}
		}
		min_dist = Clamp (1f-min_dist, 0f, 1f);
		//min_dist = (float)Math.sin(min_dist);
	//	min_dist = 1f-min_dist;
		min_dist = (float)Math.sin(min_dist);
		min_dist = min_dist * (x[2][myAtom] * x[2][myAtom]);
		
	//	min_dist = 1.0f - (min_dist * 0.15f);
	//	min_dist = 1.0f -(min_dist*0.15f);
	//	min_dist = (-((float)Math.cos((-PI * min_dist)) * x[2][myAtom] * PI) + ((float)Math.sin((0.0f - modY) * PI + (PI * 0.5f))))*min_dist;
	//	_dist[k] = Clamp(min_dist/4.15f, 0f, 10f);
		_dist[k] = min_dist;
	}
	outlet(0,_dist);
	return 0;
}

int calc_distSimple()
{
	float[] _dist = new float[n_z];
	for(int k = 0; k < n_z; k++)		// �ber alle punkte z
	{
		_dist[k] = 0.5f;	
	}
	outlet(0,_dist);
	return 0;
}

int calc_z()
{
		for(int i = 0; i < n_z; i++)
		{
			z[0][i] = i*d_z*(float)Math.sin(fi);
			z[1][i] = i*d_z*(float)Math.cos(fi);
//			post("z0 "+z[0][i]+" z1 "+z[1][i]);
		}
	return 0;
}

float Clamp(float val, float min, float max) {
    return Math.max(min, Math.min(max, val));
}

double Distance_minkowski(double ax, double ay, double bx, double by, double mix)
{
    return Math.pow(Math.pow(Math.abs(ax - bx), mix) + Math.pow(Math.abs(ay - by), mix), 1.0f / mix);
}

}



























