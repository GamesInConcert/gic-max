# Games In Concert (Unity Project)

Games in concert is a SNF funded research project exploring the possibilities and implications of collaborative creativity in virtual environments.
This is the Main Repository for the Max/MSP part of the project. Max is used for the live synthesis and spatialization of the instruments.

For the Unity3D part please visit: (https://gitlab.zhdk.ch/GamesInConcert/gic-unity)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To sucessfully run the Max/MSP project the following dependencies have to be installed.
Max Externals:
* [ICST Ambisonics](https://www.zhdk.ch/downloads-ambisonics-externals-for-maxmsp-5381) - A set of externals for MaxMSP for Ambisonics surround sound processing and source-control in three dimensions.
* [Sadamlib](http://sadam.hu/hu/node/1) - The sadam Library is a set of free externals including some very good networking items.
 
VST Plugins and Other:
* [Ambix VST Plugin (1st order)](http://www.matthiaskronlachner.com/?p=2015) - The plugin used to Render the sound binaural to your headphones.
* [Ambix Binaural Presets](http://www.matthiaskronlachner.com/?p=2015) - Binaural Presets.
* [JDK V8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - For some java externals used.

### Installing

To Get the Project running: 
* Clone this repo (or download it).
* Install All Dependencies.
* Open any _GIC_FinalMasterXXX patcher. (Found in each top level folder)

You should now have a working patcher wothout any errors.

## Running the Patcher

### Setup

* Start the _GIC_FinalMasterROLE Max-Patch depending on your Role: Seaboard, Trees, Paint or GJ (This might take some while)
* Select the appropriate audio output driver (e.g. HTC Vive) in the audio options (or double click on the "dac~ 12"). 
* Set the Ambix Preset to Square by clicking on the "set Plugin Preset to Square" button and selecting the "Square" preset.
* Turn the Audio Engine on
* Click on the "Turn YourPlayerRole ON" button. 
* Hit Play in the Unity Project
* (GJ Only) Mix the "GJ Volume" to your liking.

Note: The Trees, Paint and Seaboard sounds are downmixed to a binaural stereo representation. The GJ output is in 7.1 Surround, you`d need the appropriate audio setup to experience the fill potential there.

### Playing

* All actions from unity should now be sonified through the patch.
* If you want to reset everything click on "All OFF"

## Compatibility:

The Patcher has been extensively tested on Windows 10 x64 and MacOS

## Authors

* **Olav Lervik** - *Concept & Audio Programming* - [Homepage](http://olavlervik.com/)
* **Simon Pfaff** - *Concept, Framework and Shaders* - [Bleep-O-Matic](http://www.bleep-o-matic.com)
* **Reto Spoerri** - *Concept & Input & Networking* - [Ludic GmbH](http://www.ludic.ch/)

## License

This project is licensed under the CC BY-SA 4.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Libraries Created during the project

* [OpusDotNet](https://github.com/ludos1978/OpusDotNet) - Implementation of the Opus Codec in Unity3D
* NetXR Library - A comprehensive framework for VR networked interactions and data transfer in Unity3D - Coming Soon

## Acknowledgments

* [Ambix VST Plugins](http://www.matthiaskronlachner.com/?p=2015) - An awesome set of VST plugins by [Matthias Kronlachner](http://www.matthiaskronlachner.com) to produce a binaural representation of an Ambisonics audio stream (And so much more). Thanks a lot!
* [ICST Ambisonics](https://www.zhdk.ch/downloads-ambisonics-externals-for-maxmsp-5381) - A set of externals for MaxMSP for Ambisonics surround sound processing and source-control in three dimensions by the [Institute of Computer sound and Technology](https://www.zhdk.ch/icst) Thanks a lot!
* [Sadamlib](http://sadam.hu/hu/node/1) - Another awesome set of max externals including some very good networking pieces. Created by [Ádám Siska](http://www.sadam.hu) Thanks man!